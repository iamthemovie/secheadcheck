# Summary

HTTP Header checker for security headers. Give it a line delimited file of URLs and it will check the basic headers for presence, depending which header and if it should or should not be present the output will show status of the URL against each header

# Install

The latest version of Node will be required (including npm)

Clone the directory or download the folder and using your command line in the directory `npm install`

# Usage

Line delimted file required for URLs, see example in repository

`node checker --file urls.txt`
