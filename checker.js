/**
 * Quick script to parse http headers from URLs and check them against a basic
 * set of rules
 */

const Table = require('cli-table');
const Request = require('request-promise-native');
const Colors = require('colors');
const CommandLineArgs = require('command-line-args')

// parse command line options
const optionDefinitions = [
    { name: 'file', type: String, multiple: false },
]

const options = CommandLineArgs(optionDefinitions)

// exit if no file is set
if (typeof options.file === 'undefined') {
    console.log('Please specify a file path for reading URLs, line delimited.');
    process.exit(0);
}

// These are the required headers
const requiredHeaders = [
    { 
        header:"Cache-Control",
        presence: true,
    },
    { 
        header:"Content-Security-Policy",
        presence: true,
    },
    { 
        header:"Strict-Transport-Security",
        presence: true,
    },
    { 
        header:"X-AspNetMvc-Version",
        presence: false,
    },
    { 
        header:"X-AspNetVersion",
        presence: false,
    },
    { 
        header:"X-Content-Type-Options",
        presence: true,
    },
    { 
        header:"X-Frame-Options",
        presence: true,
    },
    { 
        header:"X-XSS-Protection",
        presence: true,
    },
]


// create the map from the URLS
let createMap = (data) => data.map((value) => {
    // make a full get request with full response for headers
    let options = {
        url: value,
        method: 'GET',
        resolveWithFullResponse: true
    };

    let req = 
        Request(options)
            .then((response) => {
                // return the url and the headers
                return {
                    url: options.url,
                    headers: response.headers
                }
            })
            .catch((err) => {
                // errors may still return headers
                if(typeof err['response'] === 'undefined') {
                    return
                }

                return {
                    url: options.url,
                    headers: err['response'].headers,
                }
            })


    return req;
});

let parseResults = (results) => results.map(v => parseSingleResult(v))

let parseSingleResult = (result) => {
    let returnArray = [];
    // map the result headers to lowercase
    Object
        .keys(result.headers)
        .reduce((c, k) => (c[k.toLowerCase()] = result.headers[k], c), {});
    
    // table result object
    // let tableItme = {
    //     url: result.url,
    //     header: 'headername',
    //     value: 'value',
    //     valid: false
    // }
    // loop through all the required headers
    for (rh in requiredHeaders) {
        let rhInfo = requiredHeaders[rh];
        let resultHeader = result.headers[rhInfo.header.toLowerCase()];
        let checkItem = {
            url: result.url,
            header: rhInfo.header.toLowerCase(),
            value: resultHeader,
            valid: false
        }
        
        // if we do not expect it to be here then make sure it isn't
        // this is valid
        if (rhInfo.presence == false && typeof resultHeader === 'undefined') {
            checkItem.valid = true;
        }

        if (rhInfo.presence && typeof resultHeader !== 'undefined') {
            checkItem.valid = true
        } 

        returnArray.push(checkItem)
    }

    return returnArray;
}

try 
{
    // lines from the file
    let lines = 
        require('fs')
            .readFileSync(options.file)
            .toString()
            .split('\n')
            .map(i => i.trim())
            .filter(i => i.length > 0) 

    // off we go    
    Promise
        .all(createMap(lines))
        .then(r => r.filter(i => i))
        .then(r => parseResults(r))
        .then(items =>
        {
            // items are arrays of each url's headers
            let table = items.map(item => 
            {
                let t = new Table({ head: ['', 'Valid'.white, 'Header Value'.white] });

                item.map(h => {
                    let obj = {}

                    let valid = h.valid ? ' Valid '.bgGreen : ' Invalid '.bgRed;

                    obj[h.header.white] = [valid, `${h.value}`.white]

                    t.push(obj)
                })
                console.log('-----------------------------------------------')
                process.stdout.write('Testing URL: ')
                console.log(` ${item[0].url} `.bgWhite.black)
                console.log(t.toString())
            })
        });
}
catch(err) {
    // catch all and log to the console
    console.log(err.toString())
}



